module.exports = {
  url: function() {
    return this.api.launchUrl
  },
  elements: {
    requestInviteButton: '#requestInviteButton',
	  inputName: 'input[name="name"]',
	  inputEmail: 'input[name="email"]',
	  inputConfirmEmail: 'input[name="confirmEmail"]',
    inviteSend: '#inviteSend',
    serverErrorMessage: '#serverErrorMessage',
  },
}
