module.exports = {
  'It submits with valid details': (browser) => {
    var signupPage = browser.page.signup()

    signupPage
      .navigate()
      .expect.element('body').text.to.contain('A better way')

    signupPage
      .click('@requestInviteButton')
      .expect.element('body').text.to.contain('Request an invite')

    signupPage
      .setValue('@inputName', 'Test')
      .setValue('@inputEmail', 'test@test.com')
      .setValue('@inputConfirmEmail', 'test@test.com')
      .click('@inviteSend')
      .waitForElementNotPresent('@inviteSend', 5000)
      .expect.element('body').text.to.contain('Thanks')

    browser.end()
  },

  'It triggers error when email already used': (browser) => {
    var signupPage = browser.page.signup()

    signupPage
      .navigate()
      .click('@requestInviteButton')
      .setValue('@inputName', 'Test')
      .setValue('@inputEmail', 'usedemail@airwallex.com')
      .setValue('@inputConfirmEmail', 'usedemail@airwallex.com')
      .click('@inviteSend')
      .waitForElementPresent('@serverErrorMessage', 5000)
      .expect.element('body').text.to.contain('Email is already in use')

    browser.end()
  },
}
