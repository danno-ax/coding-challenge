// @flow

import React from 'react'

import R from 'ramda'
import axios from 'axios'
import {
  hasError,
  isIncomplete,
  validate,
} from './SignupButton/services.js'


import Modal from 'react-modal'

import { Form } from './SignupButton/Form.jsx'
import { Send } from './SignupButton/Send.jsx'

import styles from './SignupButton.css'

export type SignupErrorsType = {
  confirmEmail: ?string,
  email: ?string,
  name: ?string,
}

export type SignupDataType = {
  confirmEmail: string,
  email: string,
  name: string,
}

type PropsType = {
}

type StateType = {
  confirmationOpen: boolean,
  error: string,
  signupData: SignupDataType,
  signupErrors: SignupErrorsType,
  signupOpen: boolean,
}

export class SignupButton extends React.Component<void, PropsType, StateType> {

  props: PropsType
  state: StateType

  constructor() {
    super()
    this.state = this.getCleanState()
  }

  getCleanState(): StateType {
    return {
      confirmationOpen: false,
      error: '',
      signupData: {
        confirmEmail: '',
        email: '',
        name: '',
      },
      signupErrors: {
        confirmEmail: null,
        email: null,
        name: null,
      },
      signupOpen: false,
    }
  }

  handleChange = (field: string, value: string) => {
    const {
      state,
    } = this

    const signupDataLens = R.lensPath(['signupData', field])
    const newState = R.set(signupDataLens, value, state)

    this.setState(newState)
  }

  handleSend = () => {
    const {
      state: {
        signupData,
      }
    } = this

    const signupErrors = validate(signupData)

    if (hasError(signupErrors)) {
      this.setState({
        signupErrors,
      })

      return
    }

    if (isIncomplete(signupData)) {
      return
    }

    axios.post('https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth', signupData)
    .then((response) => {
      let newState = this.getCleanState()
      newState.signupOpen = false
      newState.confirmationOpen = true

      this.setState(newState)
    })
    .catch((error) => {
      const {
        response: {
          data,
        }
      } = error

      this.setState({
        error: data.errorMessage,
      })
    })
  }

  handleModalStateEvent = R.curry((name: string, newState: boolean, _event: SyntheticInputEvent) => {
    this.setState({
      [name]: newState,
    })
  })

  render(): React.Element<*> {
    const {
      state: {
        confirmationOpen,
        error,
        signupData,
        signupErrors,
        signupOpen,
      }
    } = this

    const renderedError = (
      <p
        className={styles.errorMessage}
        id='serverErrorMessage'
      >
        {error}
      </p>
    )

    return (
      <div>
        <button
					id='requestInviteButton'
					onClick={this.handleModalStateEvent('signupOpen', true)}
				>
          Request an invite
        </button>

        <Modal
					className={styles.modal}
				  overlayClassName={styles.overlay}
          contentLabel="Signup modal"
          isOpen={signupOpen}
          onRequestClose={this.handleModalStateEvent('signupOpen', false)}
        >
          <p className={styles.instruction}>Request an invite</p>
          <Form
            onChange={this.handleChange}
            onSubmit={this.handleSend}
            signupData={signupData}
            signupErrors={signupErrors}
          />
          {error !== '' ? renderedError : null}
          <Send
            onClick={this.handleSend}
            signupData={signupData}
            signupErrors={signupErrors}
          />
        </Modal>

        <Modal
					className={styles.modal}
				  overlayClassName={styles.overlay}
          contentLabel="Confirmation modal"
          isOpen={confirmationOpen}
          onRequestClose={this.handleModalStateEvent('confirmationOpen', false)}
        >
          <p className={styles.instruction}>Thanks, we will be in touch shortly</p>
          <button onClick={this.handleModalStateEvent('confirmationOpen', false)}>
            Close
          </button>
        </Modal>
      </div>
    )
  }
}
