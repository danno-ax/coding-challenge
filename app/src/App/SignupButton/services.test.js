import { hasError, isIncomplete, validate } from './services.js'

describe('.validate', () => {
  test('tests confirmEmail matching', () => {
    const obj = {
      email: 'test@test.com',
      confirmEmail: 'test@test.com'
    }

    expect(validate(obj)).toEqual({
      email: null,
      confirmEmail: null,
    })
  })

  test('tests confirmEmail not matching', () => {
    const obj = {
      email: 'test@test.com',
      confirmEmail: 'test2@test.com'
    }

    expect(validate(obj)).toEqual({
      email: null,
      confirmEmail: 'Confirmation email must match email',
    })
  })
})

describe('.hasError', () => {
  test('true when error in object', () => {
    const obj = {
      one: null,
      two: 'I am an error',
    }

    expect(hasError(obj)).toBe(true)
  })

  test('false when no error in object', () => {
    const obj = {
      one: null,
      two: null,
    }

    expect(hasError(obj)).toBe(false)
  })
})

describe('.isIncomplete', () => {
  test('true when field is empty', () => {
    const obj = {
      one: '',
      two: 'I have a value',
    }

    expect(isIncomplete(obj)).toBe(true)
  })

  test('false when fields are complete', () => {
    const obj = {
      one: 'Also a value',
      two: 'I have a value',
    }

    expect(isIncomplete(obj)).toBe(false)
  })
})
