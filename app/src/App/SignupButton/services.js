import R from 'ramda'
import emailValidator from 'email-validator'

import type { SignupDataType } from '../SignupButton.jsx'
import type { SignupErrorsType } from '../SignupButton.jsx'

export const validate = (data: SignupDataType): SignupErrorsType =>
  R.pipe(
    R.toPairs,
    R.map(([field, value]: [string, string]): [string, ?string] => {

      switch (field) {
        case 'confirmEmail':
          if(value !== data.email) {
            return [field, 'Confirmation email must match email']
          }
          break;

        case 'email':
          if(!emailValidator.validate(value)) {
            return [field, 'Email is not correct format']
          }
          break;

        case 'name':
          if(value.length < 3) {
            return [field, 'Name must be longer than 3 characters']
          }
          break;
      }

      return [field, null]
    }),
    R.fromPairs,
  )(data)

export const hasError = R.pipe(
  R.toPairs,
  R.map(R.nth(1)),
  R.any(R.is(String)),
)

export const isIncomplete = R.pipe(
  R.toPairs,
  R.map(R.nth(1)),
  R.any((value) =>
    value === ''
  ),
)
