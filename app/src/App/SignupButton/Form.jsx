// @flow

import React from 'react'

import R from 'ramda'

import { Field } from './Form/Field.jsx'

import styles from './Form.css'

import type { SignupDataType } from '../SignupButton.jsx'
import type { SignupErrorsType } from '../SignupButton.jsx'

type PropsType = {
  onChange: (field: string, value: string) => void,
  onSubmit: () => void,
  signupData: SignupDataType,
  signupErrors: SignupErrorsType,
}

const handleChange = R.curry((props: PropsType, field: string, event: SyntheticInputEvent) =>
  props.onChange(field, event.target.value)
)

export const Form = (props: PropsType) => {
  const {
    onSubmit,
    signupErrors,
    signupData,
  } = props

  return (
    <form onSubmit={onSubmit}>
      <div className={styles.field}>
        <Field
          error={signupErrors.name}
          label='Full name'
          name='name'
          onChange={handleChange(props, 'name')}
          value={signupData.name}
        />
      </div>
      <div className={styles.field}>
        <Field
          error={signupErrors.email}
          label='Email'
          name='email'
          onChange={handleChange(props, 'email')}
          value={signupData.email}
        />
      </div>
      <div className={styles.field}>
        <Field
          error={signupErrors.confirmEmail}
          label='Confirm email'
          name='confirmEmail'
          onChange={handleChange(props, 'confirmEmail')}
          value={signupData.confirmEmail}
        />
      </div>
    </form>
  )
}
