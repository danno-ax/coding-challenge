// @flow

import React from 'react'

import classNames from 'classnames'

type PropsType = {
  error: ?string,
  label: string,
  name: string,
  onChange: (field: string, value: string) => void,
  value: string,
}

import styles from './Field.css'

export const Field = (props: PropsType) => {
  const {
    error,
    label,
    name,
    onChange,
    value,
  } = props

  const classes = classNames({
    [styles.root]: true,
    [styles.error]: !!error,
  })

  const renderedError = (
    <span className={styles.error}>{error}</span>
  )

  return (
    <label className={classes}>
      <input
        name={name}
        onChange={onChange}
        placeholder={label}
        type='text'
        value={value}
      />
      {!!error ? renderedError : null}
    </label>
  )
}
