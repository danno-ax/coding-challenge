// @flow

import React from 'react'
import ReactDOM from 'react-dom'

import { SignupButton } from './App/SignupButton.jsx'

import styles from './App.css'

const mountNode = document.createElement('div')
mountNode.setAttribute('id', 'app')

if (!document.body) {
  throw new Error('Unable to mount application, no document body found')
}

document.body.appendChild(mountNode)

ReactDOM.render(
  <div className={styles.root}>
    <header>
      <div>
        Broccoli & Co
      </div>
    </header>
    <main>
      <div className={styles.panel}>
        <div className={styles.introduction}>
          <h1>A better way to enjoy every day</h1>
          <h2>Be the first to know when we launch</h2>
        </div>
        <div className={styles.signupButton}>
          <SignupButton />
        </div>
      </div>
    </main>
    <footer>
      Made with❤️ in Melbourne<br />
      &copy; {(new Date).getFullYear()} Broccoli &amp; Co. All rights reserved
    </footer>
  </div>,
  mountNode,
)
